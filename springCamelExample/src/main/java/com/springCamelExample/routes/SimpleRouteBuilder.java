package com.springCamelExample.routes;


import org.apache.camel.builder.RouteBuilder;

import com.springCamelExample.processor.MyProcessor;

public class SimpleRouteBuilder extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("configureArriba");
		from("file:input?noop=true").process(new MyProcessor()).to("file:output");
		System.out.println("configure");
		
	}


}
