package com.springCamelExample.routes;

//import org.apache.camel.CamelContext;
//import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;

public class DirectRoute extends RouteBuilder {


	@Override
	public void configure() {
		from("direct:start").to("seda:end");
	}
}
