package com.springCamelExample.main;

import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;
//import org.springframework.context.support.AbstractApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.springCamelExample.routes.DirectRoute;
//import com.springCamelExample.routes.SimpleRouteBuilder;

public class MainApp {

	public static void main(String[] args) {
		//AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		CamelContext ctx = new DefaultCamelContext();
		
		ProducerTemplate prod = ctx.createProducerTemplate();
		ConsumerTemplate consu = ctx.createConsumerTemplate();
		try {
			
			ctx.addRoutes(new DirectRoute());
			System.out.println("App Started");
//		while(true) {
			ctx.start();
			System.out.println("ctx");
//			ctx.addRoutes(new SimpleRouteBuilder());
			
			
			prod.sendBody("direct:start", "Hello Aliens! This body was sent from the producer");
//			String msg = consu.receiveBody("file:output",String.class);
//			Thread.sleep(1000);
			String msg2 = consu.receiveBody("seda:end",String.class);
//			System.out.println(msg+"1");
			System.out.println(msg2+"2");
//		}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}//Cierre MAIN
		/**
		try {
            Thread.sleep(60*1000);
            System.out.println("termino el clock");
		}
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
        	System.out.println("Cierre");
			ctx.stop();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
	}
	*/

	
